# anon: Alternate syntaxes for creating anonymous functions

Create functions by either 

1. Abusing the formulae syntax.
2. Using an operator.

This is intended to make it easier to write inline anonymous functions (which is useful when calling `lapply()`, for example).

The `f()` function take a formula input. The left hand side of the formula describes the arguments (separated by `+`), and the right hand side describes the body of the function. This function calculates the sine of `x + y`. 


```r
# Traditional
function(x, y) sin(x) * cos(y) + cos(x) * sin(y)
```

```
## function(x, y) sin(x) * cos(y) + cos(x) * sin(y)
```

```r
# Formula
f(x + y ~ sin(x) * cos(y) + cos(x) * sin(y))
```

```
## function (x, y) 
## sin(x) * cos(y) + cos(x) * sin(y)
## <environment: 0x1066b52d0>
```

The alternative approach is to use the `%F%` operator.  Here, the left hand side of the operator takes the arguments to the function, which are wrapped in a call to `a()`. The right hand side describes the body of the function, which is wrapped in `b()`. Note that you need to write `=` after the name of each argument, even if there is no default.


```r
# Operator
a(x =, y =) %F% b(sin(x) * cos(y) + cos(x) * sin(y))
```

```
## function (x, y) 
## sin(x) * cos(y) + cos(x) * sin(y)
## <environment: 0x10634e190>
```

In this first example, where there are no default values for arguments, the formula interface is superior.

Here's another example, calculating a hypotenuse, that sets default values for the arguments.  To provide default values for arguments using the formula interface, wrap them in `I()`.


```r
# Traditional
function(x = 3, y = 4) x ^ 2 + y ^ 2
```

```
## function(x = 3, y = 4) x ^ 2 + y ^ 2
```

```r
# Formula
f(I(x = 3) + I(y = 4) ~ x ^ 2 + y ^ 2)
```

```
## function (x = 3, y = 4) 
## x^2 + y^2
## <environment: 0x102ff3d40>
```

```r
# Operator
a(x = 3, y = 4) %F% b(x ^ 2 + y ^ 2)
```

```
## function (x = 3, y = 4) 
## x^2 + y^2
## <environment: 0x108a50518>
```

In this case, the formula interface is a little clunkier, but the operator interface take the same space as the traditional interface.
